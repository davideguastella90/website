+++
author = "Davide"
title = "TORRES"
+++

![Logo](/images/logo-no-background.png)

#### News



#### Introduction

The TORRES project focuses on the design and development of a framework for monitoring and analysis of traffic data at the scale of a large city, with Brussels as a typical use case. Thanks to a joint effort from both academic and industrial partners, the project goal will output new, groundbreaking solutions for energy- and privacy-aware traffic monitoring. 

The project aims at:
*   Enriching knowledge about the dynamics of urban mobility in the Brussels region through the integration of real data, collected from existing monitoring infrastructures and opportunely anonymized, and synthetic data created through data augmentation methods.
*   Aggregating raw mobility data acquired from IoT-connected devices and from existing monitoring infrastructures to infer information useful for mobility policymakers.
*	Creating new Artificial Intelligence-based methods for interpolating mobility data considering the uncertainties and unpredictable dynamics of the physical environment.
*	Develop the necessary dashboards and frameworks for traffic analysis, monitoring, and prediction on the scale of a metropolitan city such as Brussels.


Researchers involved (ULB):
*	Prof. Gianluca Bontempi - MLG - Computer Science Department, ULB 
*   Dr. Davide A. Guastella - MLG - Computer Science Department, ULB 

Partners:
* Université Libre de Bruxelles, Machine Learning Group (MLG youtube channel)
*	Vrije Universiteit Brussel, Electronics and Informatics Department (ETRO)
*	Macq

Funding: Innoviris
 
Duration: 2023-2025

#### Related Bibliography
