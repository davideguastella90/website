+++
author = "Hugo Authors"
title = "Research"
+++

The smart city is interested in improving the quality of life of its inhabitants. In order to be closer to reality, a smart city requires regular observation of its environment through sensors for a better knowledge of human activities and the conditions in which these activities are carried out. On the other hand, even if these sensors are often cheap, their installation and maintenance costs increase rapidly with their number. The purpose of our work is to learn the state of the environment of such a system without having the need to deploy a lot of sensors. In this way the costs related to the management of the sensor infrastructure can be therefore reduced by the proposed solution.

In my thesis work, which is part of the neOCampus operation, we assume that the number of people transiting daily on the Paul Sabatier University campus amounts to about 20.000 and that 80% of them have a smartphone which allows the acquisition of more than 500.000 daily data. We want to use a cooperative multi-agent approach to estimate environmental information by using heterogeneous, partial and intermittent information coming from the smartphones of the users of the university Paul Sabatier. The use of different heterogeneous devices should allow the multi-agent system to determine as accurately as possible the environmental values (temperature, brightness, humidity, noise ...) but also to deduce the state of the surrounding devices (lights, shutters, doors, ...) anywhere in the campus in order to guarantee a good level of comfort to users.

In my **first contribution** I focused on a technique to limit the number of sensors to deploy using standard convolution and clustering techniques to estimate environmental information where ad hoc sensors are absent or not available due to a failure.


In my **second contribution** I developed a cooperative multi-agent system to estimate homogeneous environmental values at particular locations where ad hoc sensors are not available. The estimation involves information from nearby real sensors. With respect to the first contribution, the multi-agent system allows to use partial information coming from intermittent and mobile devices.

In my **third contribution** I developed a cooperative multi-agent system to estimate homogeneous environmental values at particular locations where ad hoc sensors are not available using historical data. This solution is based on a multi-agent cooperative system where each agent exploits data windows containing time-consecutive information, called *Ambient Context Windows* (ACW), in order to find recurring dynamics in the historical data.

The implemented techniques do not make any assumption on the scale. We use information from both medium scale (the campus of the university Toulouse III Paul Sabatier) and large scale (Emilia-Romagna region in Italy).



# Publications

#### Estimating Missing Environmental Information by Contextual Data Cooperation (conference paper)

In book: PRIMA 2019: Principles and Practice of Multi-Agent Systems. 28-31 October, 2019. Torino (Italy)

**Authors:** Davide Andrea Guastella, Valérie Camps (SMAC-IRIT), Marie-Pierre Gleizes (SMAC-IRIT)
 
* DOI: 10.1007/978-3-030-33792-6_37
* Link: https://link.springer.com/chapter/10.1007/978-3-030-33792-6_37

#### Multi-Agent Systems for Estimating Missing Information in Smart Cities (conference paper)

In 11th International Conference on Agents and Artificial Intelligence (ICAART 2019). Prague (Czech Republic)

**Authors:** Davide Andrea Guastella, Valérie Camps (SMAC-IRIT), Marie-Pierre Gleizes (SMAC-IRIT)

* DOI: 10.5220/0007381902140223
* Link: http://www.scitepress.org/DigitalLibrary/Link.aspx?doi=10.5220/0007381902140223

---

#### Estimating Missing Information by Cluster Analysis and Normalized Convolution (conference paper)

In IEEE International Forum on Research and Technology for Society and Industry (RTSI 2018), Palermo (Italy)

**Authors:** Davide Andrea Guastella (SMAC-IRIT), Cesare Valenti (University of Palermo, Italy)

* DOI: https://doi.org/10.1109/RTSI.2018.8548454
* link: https://ieeexplore.ieee.org/abstract/document/8548454

---

#### Cartoon Filter via Adaptive Abstraction (regular paper)

In Journal of Visual Communication and Image Representation 36:149-158, 2016

**Authors:** Davide Andrea Guastella (SMAC-IRIT), Cesare Valenti (University of Palermo, Italy)

* DOI: https://doi.org/10.1016/j.jvcir.2016.01.012
* source code/demo: http://math.unipa.it/cvalenti/adaptiveabstraction/

---


# Invited Seminars

**24/09/2019** – Technology for the Greater Good - Virtual Sensors Turning on Air Cons, Sustainable Buildings Research Centre (SBRC), Wollongong (Australia)

**11/09/2019** – Technology for the Greater Good - Virtual Sensors Turning on Air Cons, SMART Infrastructure Facility, Wollongong (Australia)

---


# Seminars and Other Scientific Activities

#### SEMINARS

* 30/1/2019 - Introduction to JavaFX, IRIT, Toulouse (France)

* 7/3/2018 - Scheduling of task of plans, IRIT, Toulouse (France)

---

#### PRESENTATION IN INTERNATIONAL CONFERENCES

* 11/2019 - Estimating missing environmental information by contextual data cooperation, Principles and Practice of Multi-Agent Systems (PRIMA)

* 02/2019 - Multi-agent systems for estimating missing information in smart cities, 11th International Conference on Agents and Artificial Intelligence (ICAART)

* 09/2018 - Estimating missing information by cluster analysis and normalized convolution, IEEE 4th International Forum on Research and Technology for Society and Industry (RTSI)

---

#### PROGRAM COMMITTEE MEMBER

* 2020 - Member of the program committee of the COGNITIVE 2020 conference.

---

#### REVIEWER OF JOURNAL ARTICLES

* 2020 - Additional reviewer for an article submitted to the Francophone Day on Multi-Agent Systems (JFSMA)

* 2018 - Additional reviewer for the Journal of Healthcare Engineering

* 2018 - Additional reviewer for the journal "Image Segmentation Techniques for Healthcare Systems" (special issue in "Journal of Healthcare Engineering")

---

#### ADMINISTRATIVE RESPONSIBILITIES

* 2019 - Substitute in the research commission of the college of doctoral students of the University Toulouse III Paul Sabatier

---

#### CONGRESS ORGANISATION

* 07/2019 - Organization of the sixth neOCampus Science Day, 10/7/2019, 100 participants

* 07/2019 - Organizing committee of the Platform of Artificial Intelligence (PFIA), ~500 participants

* 07/2019 - Organizing committee of the Journées Francophones sur les Systèmes Multi-Agents (JFSMA), ~100 participants

---

#### OTHER

* 2020 - Organization of the writing seminar for the SMAC team, 24/3/2020

* Since 2019 - Responsible for the organization of ICIcafé seminars for the SMAC team

* 03/2019 - Participation in the "Math on Stage" day (Salle du Lac, Castanet-Tolosan) as a speaker on the introduction to LEGO Mindstorm programming for high school students (14/3/2019).

---

#### INTERNSHIPS

* Tutor of a group of 4 students (level M1-Info) for a Research Initiation Work (TIR) entitled "Self-adaptive systems in smart cities". The objective of this study is to review the main technologies used for smart city design, identify and classify them according to the self-adapting properties of the systems that realize them.

* Stage tutor of a Licence 2 student. The objective of the stage was to develop an Android application for acquiring ambient data from the environment and collect them into the neOCampus server.