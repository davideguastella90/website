+++
author = "Davide"
title = "HybridIoT"
+++

#### News

**[January 2021]** The **Toulouse Tech Transfer** (TTT) has financed with 12.000€ to develop a first industrial prototype of the HybridIoT project. 

#### Introduction

*The concept behing of HybridIoT is simple: providing **more** information with **less** physical sensors.*

Today, significant socio-economic changes are being driven by sustainable development, to consume energy resources responsibly. Governmental organizations can achieve the goal of sustainable development through exploitation of data from IoT sensors deployed in large-scale urban environment. However, the deployment of such sensor infrastructures require significant maintenance and installation cost.

<!---Moreover, sensors require a relevant network infrastructure, which entails installation costs, to cover new regions of the environment and replacement in case of failure or breakdown.-->

![Example image](/images/method.gif)

HybridIoT is an agent-based approach that blends physical and virtual sensors to provide valuable information for the decision-making process in smart cities and reduce the deployment of physical sensors. Virtual sensors are software agents that act like physical sensors and estimate missing values by using values perceived by nearby available sensors (either virtual or physical) or historical data. Software agents must estimate missing values as they are not associated with any physical instrumentation capable of providing information from direct observation of the environment.


<!---The concept of *Smart City* has spread as a solution to ensure better access to information and services to citizens, but also as a means to reduce the environmental footprint of cities. To this end, a continuous and wide observation of the environment is necessary to analyze information that enables government bodies to act on the environment appropriately. Moreover, a diffused acquisition of information requires adequate infrastructure and proper devices, which results in relevant installation and maintenance costs.

Thanks to their increasing computational power and accessibility, smart devices can be exploited to make the data acquisition in cities a participatory activity. This is the key concept of *Mobile Crowd Sensing* (MCS), which leverages device mobility and sensing capabilities, as well as human collaboration and intelligence to distributively perform tasks and provide cost-efficient applications and services. MCS allows integrating different types of smart devices into a large scale sensing infrastructure. However, on a large scale context, thousand of devices can be present; among these, several sensors could face unpredictable situations in which information has to be estimated and provided to both users and experts. Also, different parts of the environment may not be sufficiently covered by sensors. 
-->
<!-- smart devices can embed a limited set of sensors: in this case, it is necessary to compensate for the lack of data through a mechanism for estimating the missing information.-->



<!-- My thesis work focuses on the conception and the development of a *Multi-Agent System* (MAS) that enables coping with the lack of environmental information in areas of the environment not covered by sensors. -->

<!--The HybridIoT system allows estimating environmental information whereas sensors are missing or temporalely unavailable due to an unpredictable malfunction. -->


<!--HybridIoT is a *Multi-Agent System* (MAS) that implements a technique for estimating missing information at large scale. HybridIoT exploits both homogeneous and heterogeneous information to provide accurate estimates.-->

<!-- MAS are systems composed of multiple interacting and autonomous entities known as *agents*, each one acting and sensing within a common environment. Agents have a partial view of the environment, they act jointly to produce a result for a goal that cannot be achieved individually.-->

The novelty of HybridIoT lies in:

* a technique for estimating missing environmental information at large scale whereas physical sensors are not available;
* limiting the installation of a large number of physical sensors by using virtual sensors;
* propose a technique to estimate missing data by integrating heterogeneous information.

The benefits of the HybridIoT compared to the state of the art techniques consist of the following properties:

* **openness**: refers to the capacity of handling new input at any time without the need for any reconfiguration;
* **large scale**: the system can be deployed in a large, urban context and ensure correct operation with a significative number of devices;
* **heterogeneity**: the system handles different types of information without any *a priori* configuration.

The following video shows the operation of the HybridIoT system in a real application case using data acquired from weather stations located on a regional scale in the Emilia-Romagna region (Italy). 

{{< youtube pEZ6Qb7YjuU>}}

#### Related Bibliography

* D. A. Guastella, V. Camps, and M.-P. Gleizes, “Estimation de données environnementales manquantes sans déploiement de capteurs supplémentaires : le système HybridIoT,” in 30èmes Journées Francophones sur les Systèmes Multi-Agents (JFSMA 2022), Jun 2022, Saint-Etienne, France.

* D. A. Guastella, V. Camps, and M.-P. Gleizes, “Estimating Missing Environmental Information by Contextual Data Cooperation,” in PRIMA 2019: Principles and Practice of Multi-Agent Systems, 2019, pp. 523–531, doi: 10.1007/978-3-030-33792-6_37.

* D. A. Guastella, V. Camps, and M.-P. Gleizes, “Multi-agent Systems for Estimating Missing Information in Smart Cities,” in Proceedings of the 11th International Conference on Agents and Artificial Intelligence - Volume 2: ICAART, 2019, pp. 214–223, doi: 10.5220/0007381902140223.

* D. A. Guastella and C. Valenti, “Estimating Missing Information by Cluster Analysis and Normalized Convolution,” in IEEE 4th International Forum on Research and Technology for Society and Industry (RTSI), Palermo, Italy, Sep. 2018.
