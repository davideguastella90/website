+++
author = "Hugo Authors"
title = "Research"
+++

# Publications

*(Flags depict collaborations)*

### Regular papers

- **[2022] ![Example image](/images/IT.png) Linguistic and semantic layers for emergency plans**
    - Link: https://content.iospress.com/articles/intelligenza-artificiale/ia210122
- **[2021] ![Example image](/images/FR.png) ![Example image](/images/IT.png) Edge-Based Missing Data Imputation in Large-Scale Environments**
    - Link: https://www.mdpi.com/2078-2489/12/5/195
- **[2020] ![Example image](/images/FR.png) A Cooperative Multi-Agent System for Crowd Sensing Based Estimation in Smart Cities**
    - Link: https://doi.org/10.1109/ACCESS.2020.3028967
- **[2016] ![Example image](/images/IT.png) Cartoon Filter via Adaptive Abstraction**
    - link: https://doi.org/10.1016/j.jvcir.2016.01.012
    - source code/demo: http://math.unipa.it/cvalenti/adaptiveabstraction/

### Conference Paper
- **[2023] ![Example image](/images/IT.png) Adaptive Execution of Workflows in Emergency Response**
    - Link: https://shorturl.at/azG12
- **[2022] ![Example image](/images/FR.png) Estimation de données environnementales manquantes sans déploiement de capteurs supplémentaires : le système HybridIoT**
    - Link: https://www.cepadues.com/nos-articles/978/
- **[2022] ![Example image](/images/IT.png) From Textual Emergency Procedures to Executable Plans**
    - Link: https://idl.iscram.org/files/massimocossentino/2022/2410_MassimoCossentino_etal2022.pdf
- **[2021] ![Example image](/images/UK.png) Learn to Sense vs. Sense to Learn: A System Self-Integration Approach**
    - Link: https://ieeexplore.ieee.org/abstract/document/9599325/

- **[2021] ![Example image](/images/FR.png) ![Example image](/images/IT.png) ![Example image](/images/AU.png)Evaluating Correlations in IoT Sensors for Smart Buildings**
    - Link: https://www.scitepress.org/Papers/2021/102105/102105.pdf
- **[2020] ![Example image](/images/FR.png) Estimating Missing Environmental Information by Contextual Data Cooperation**
    - Link: https://link.springer.com/chapter/10.1007/978-3-030-33792-6_37
- **[2019] ![Example image](/images/FR.png) Multi-Agent Systems for Estimating Missing Information in Smart Cities**
    - Link: http://www.scitepress.org/DigitalLibrary/Link.aspx?doi=10.5220/0007381902140223
- **[2018] ![Example image](/images/IT.png) Estimating Missing Information by Cluster Analysis and Normalized Convolution**
    - link: https://ieeexplore.ieee.org/abstract/document/8548454



---

# Invited Seminars

**24/09/2019** – Technology for the Greater Good - Virtual Sensors Turning on Air Cons, Sustainable Buildings Research Centre (SBRC), Wollongong (Australia)

**11/09/2019** – Technology for the Greater Good - Virtual Sensors Turning on Air Cons, SMART Infrastructure Facility, Wollongong (Australia)

---


# Seminars and Other Scientific Activities

#### SEMINARS

* 8/12/2022 - Data imputation and traffic simulation, School of Computing, University of Leeds

* 30/1/2019 - Introduction to JavaFX, IRIT, Toulouse (France)

* 7/3/2018 - Scheduling of task of plans, IRIT, Toulouse (France)

---

#### PROGRAM COMMITTEE MEMBER

* 2020 - Member of the program committee of the COGNITIVE 2020 conference.

---

#### REVIEWER OF JOURNAL ARTICLES

* 2021 - Reviewer for GeoInformatica (Elsevier), 

* 2023 - Reviewer for IEEE ITSC'23

* 2021 - Reviewer for IEEE Access journal (Q1)

* 2020 - Reviewer for an article submitted to the Francophone Day on Multi-Agent Systems (JFSMA)

* 2018 - Reviewer for the Journal of Healthcare Engineering

* 2018 - Reviewer for the journal "Image Segmentation Techniques for Healthcare Systems" (special issue in "Journal of Healthcare Engineering")

---

#### ADMINISTRATIVE RESPONSIBILITIES

* 2019 - Substitute in the research commission of the college of doctoral students of the University Toulouse III Paul Sabatier

---

#### CONGRESS ORGANISATION

* 07/2019 - Organization of the sixth neOCampus Science Day, 10/7/2019, 100 participants

* 07/2019 - Organizing committee of the Platform of Artificial Intelligence (PFIA), ~500 participants

* 07/2019 - Organizing committee of the Journées Francophones sur les Systèmes Multi-Agents (JFSMA), ~100 participants

---

#### OTHER

* 2020 - Organization of the writing seminar for the SMAC team, 24/3/2020

* Since 2019 - Responsible for the organization of ICIcafé seminars for the SMAC team

* 03/2019 - Participation in the "Math on Stage" day (Salle du Lac, Castanet-Tolosan) as a speaker on the introduction to LEGO Mindstorm programming for high school students (14/3/2019).
