+++
title = "About"
description = "Hugo, the world’s fastest framework for building websites"
date = "2019-02-28"
aliases = ["about-us","about-hugo","contact"]
author = "Hugo Authors"
+++


<!--Currently I am a lecturer (ATER) at the University of Toulouse III - Paul Sabatier. -->


I was born in Palermo, Italy. I received both bachelor's and master's degrees in Computer Science at the University of Palermo, Italy. In 2017 I received the master's degree in Computer Science from the University of Paris Marne-la-Vallée in the context of the joint double degree program between the University of Paris Marne-la-Vallée and the University of Palermo.

<!--From january 2023 I am postdoctoral researcher at Université libre de Bruxelles with the  Machine Learning Group (MLG).

From october 2021 to october 2022 I was research associate at ICAR institute of the national research counsil of Italy (CNR).

From september 2020 to august 2021 I was ATER (part-time lecturer) at the University of Toulouse III - Paul Sabatier.-->

In december 2020 I obtained my PhD in computer science at the University of Toulouse III-Paul Sabatier. My thesis focuses on an agent-based technique for estimating missing environmental information in sensors networks at large-scale.

<!--- From september 2020 to august 2021, I've been part-time lecturer (ATER) at the University of Toulouse III-Paul Sabatier and ANITI, the institute of artificial intelligence of Toulouse.

In 2017 I worked as an internship at the Laboratoire d'Informatique de Paris 6 (LIP6) under the supervision of Amal El-Fallah Seghrouchni and Safia Kedad-Sidhoum. I developed an algorithm for scheduling plans of tasks under different constraints such as start and due date, processing time and resouces consumption.

In 2016 I worked with Cesare Valenti (Researcher at the University of Palermo, Italy). We proposed a solution that acts as an edge-preserving technique that results in a cartoon-like stylization of images. The results of the technique have been published in an international journal.

In 2015 I worked at the National Council of Research (CNR) with Massimo Cossentino and Luca Sabatucci. I contributed to the development of a workflow management system based on a multi-agent architecture called MUSA (Middleware for User driven Service Adaptation). Its objective was to enable a user to define a new cloud application such as integration (in terms of data and process) of existing cloud applications. The OCCP project was funded by the Autonomous Region of Sicily within the framework of the European Regional Development Fund (ERDF).
-->



![Example image](/images/timeline-davide.png)
---

## Education

* **2017**, Master M2 Informatique Fondamentale, University of Paris Marne-la-Vallée (Joint double degree between University of Palermo and University of Paris Marne-la-Vallée) Mention bien

* **2015-2017**, Master of Science, University of Palermo, Italy

* **2009-2014**, Bachelor of Science, University of Palermo, Italy

---

## Professional Experiences

#### Jul 2023–Sept 2023, External consultant, ICAR-CNR, Palermo, Italy

As part of the ECOS team within the CNR, my role is to provide support for preparing and deploying the demonstration prototype for the NETTUNIT project.


#### Jan 2023–ongoing, Postdoctoral researcher, Université Libre de Bruxelles, Bruxelles, Belgium

As part of the Machine Learning Group (MLG), I work in the context of the TORRES projet. The projet aims at developing ground-breaking technologies for supporting traffic simulation, analysis, and forecasting in the region of Brussels.

#### Oct 2021–Oct 2022, Research associate, ICAR-CNR, Palermo, Italy

As part of the ECOS team within the CNR, I work in the context of the NETTUNIT project, which aims at developing a cross-border tool for the management of emergencies at sea between Italy and Tunisia.

#### Aug 2019–Oct 2019, Visiting Fellow, SMART Infrastructure Facility, University of Wollongong, Australia

As visiting fellow at the Smart Infrastructure Facility at the University of Wollongong (Australia), my objective was to acquire ambient data from buildings to be tested on the system developed as part of my PhD research project. The data are used to test a software system that act directly on the environment in order to optimize energy consumption while ensuring user comfort.

#### Feb 2017--Jul 2017, Internship, Laboratoire d'Informatique de Paris 6

Development of a heuristic algorithm for solving the problem of scheduling plans of tasks. The plans are ordered vectors of tasks, and tasks are basic operations carried out by resources. Plans are tied by temporal, precedence and resource constraints that makes the scheduling problem hard to solve in polynomial time.

#### Oct 2014-Jun 2015, Fixed-term contract, National Research Council (CNR), Palermo, Italy

Development of a workflow management system based on an agent-oriented software architecture using Java and Jason (AgentSpeak). The purpose of the system is to provide a flexible middleware for the efficient composition and orchestration of services in distributed environments.

#### Oct 2013-Jan 2014, Internship,  National Research Council (CNR), Palermo, Italy

Development of an Eclipse based tool to support the PASSI methodology.
