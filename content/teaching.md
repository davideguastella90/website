+++
author = "Hugo Authors"
title = "Teaching"
+++

I was part-time lecturer at the University Toulouse III-Paul Sabatier. I have accumulated more that 150 hours of teaching activities. My teaching interest focus on programming using C, C++, Python and Java languages. I'm also interested at teaching multi-thread/multi-process programming, design pattern and multi-agent systems.

Below you find a list of teaching courses I contributed to.

## 2020-2021

* **Logique 2**, Cours-TD

    * support auxiliaire:

* **Algorithmique avancée**, TP
    * Introduction au TP2 (flots+prog. linéaire)

* **Systèmes ambiants et mobiles**, TP/TD

* **Informatique graphique, traitement et analyse d'image**, Cours-TD, TP

* **Intelligence artificielle**, Cours-TD, TP

* **Agents Intelligents**, TD, TP


## 2019-2020

* **Systèmes ambiants et mobiles**, TP/TD (Travaux Pratiques, Travaux Dirigés)

* **Composants, design patterns, composition et flexibilité**, TD

* **Conception Objet et Outillage** TP

* **Developpement et Environnement** Cours TD (Courses, TD), TP
    * Exercices d'autovaluation: 

* **Systèmes 2** TP

## 2018-2019

* **Informatique**, TP

* **Conception Objet et Outillage**, TP

* **Fondaments de la Programmation**, TP

* **Systèmes 1**, TP

## 2017-2018

* **Systèmes 2**, TP