+++
author = "Davide"
title = "Welcome"
+++

Hi, I am Davide Guastella

I am computer scientist from Palermo (Italy). Currently I'm postdoctoral researcher at Université Libre de Bruxelles (Belgium), with the Machine Learning Group (MLG)

My research interests include :

- Traffic modelling and simulation
- Multi-agent systems
- Missing data imputation
- (Meta)-heuristics
- Computer graphics

## Contact

**Current Position**: postdoctoral researcher

**Team**: machine learning group (Université Libre de Bruxelles)

**E-mail**: davide.andrea.guastella at ulb dot be

**Localisation**: campus la plaine, NO building, 8th floor, 2.O8.216