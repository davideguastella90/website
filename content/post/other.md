+++
author = "Hugo Authors"
+++

## Invited Seminars

**24/09/2019** – Technology for the Greater Good - Virtual Sensors Turning on Air Cons, Sustainable Buildings Research Centre (SBRC), Wollongong (Australia)

**11/09/2019** – Technology for the Greater Good - Virtual Sensors Turning on Air Cons, SMART Infrastructure Facility, Wollongong (Australia)


## Seminars and Other Scientific Activities

#### SEMINARS

* 30/1/2019 - Introduction to JavaFX, IRIT, Toulouse (France)

* 7/3/2018 - Scheduling of task of plans, IRIT, Toulouse (France)

---

#### PRESENTATION IN INTERNATIONAL CONFERENCES

* 11/2019 - Estimating missing environmental information by contextual data cooperation, Principles and Practice of Multi-Agent Systems (PRIMA)

* 02/2019 - Multi-agent systems for estimating missing information in smart cities, 11th International Conference on Agents and Artificial Intelligence (ICAART)

* 09/2018 - Estimating missing information by cluster analysis and normalized convolution, IEEE 4th International Forum on Research and Technology for Society and Industry (RTSI)

---

#### PROGRAM COMMITTEE MEMBER

* 2020 - Member of the program committee of the COGNITIVE 2020 conference.

---

#### REVIEWER OF JOURNAL ARTICLES

* 2020 - Additional reviewer for an article submitted to the Francophone Day on Multi-Agent Systems (JFSMA)

* 2018 - Additional reviewer for the Journal of Healthcare Engineering

* 2018 - Additional reviewer for the journal "Image Segmentation Techniques for Healthcare Systems" (special issue in "Journal of Healthcare Engineering")

---

#### ADMINISTRATIVE RESPONSIBILITIES

* 2019 - Substitute in the research commission of the college of doctoral students of the University Toulouse III Paul Sabatier

---

#### CONGRESS ORGANISATION

* 07/2019 - Organization of the sixth neOCampus Science Day, 10/7/2019, 100 participants

* 07/2019 - Organizing committee of the Platform of Artificial Intelligence (PFIA), ~500 participants

* 07/2019 - Organizing committee of the Journées Francophones sur les Systèmes Multi-Agents (JFSMA), ~100 participants

---

#### OTHER

* 2020 - Organization of the writing seminar for the SMAC team, 24/3/2020

* Since 2019 - Responsible for the organization of ICIcafé seminars for the SMAC team

* 03/2019 - Participation in the "Math on Stage" day (Salle du Lac, Castanet-Tolosan) as a speaker on the introduction to LEGO Mindstorm programming for high school students (14/3/2019).

---

#### INTERNSHIPS

* Tutor of a group of 4 students (level M1-Info) for a Research Initiation Work (TIR) entitled "Self-adaptive systems in smart cities". The objective of this study is to review the main technologies used for smart city design, identify and classify them according to the self-adapting properties of the systems that realize them.

* Stage tutor of a Licence 2 student. The objective of the stage was to develop an Android application for acquiring ambient data from the environment and collect them into the neOCampus server.
